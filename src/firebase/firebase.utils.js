import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

const config = {
    apiKey: "AIzaSyDP5Evmp1GfbuW1Yv_aHLVHiTXytUJoe40",
    authDomain: "sanguine-anthem-282814.firebaseapp.com",
    databaseURL: "https://sanguine-anthem-282814.firebaseio.com",
    projectId: "sanguine-anthem-282814",
    storageBucket: "sanguine-anthem-282814.appspot.com",
    messagingSenderId: "414926283253",
    appId: "1:414926283253:web:9b8f3bf4b85fa0433f40bc",
    measurementId: "G-2G7WWG5SJB"
  };

firebase.initializeApp(config);

export const createUserProfileDocument = async (userAuth, additionalData) => {
  if (!userAuth) return;

  const userRef = firestore.doc(`users/${userAuth.uid}`);

  const snapShot = await userRef.get();

  if (!snapShot.exists) {
    const { displayName, email } = userAuth;
    const createdAt = new Date();
    try {
      await userRef.set({
        displayName,
        email,
        createdAt,
        ...additionalData
      });
    } catch (error) {
      console.log('error creating user', error.message);
    }
  }

  return userRef;
};

export const auth = firebase.auth();
export const firestore = firebase.firestore();

const provider = new firebase.auth.GoogleAuthProvider();
provider.setCustomParameters({ prompt: 'select_account' });
export const signInWithGoogle = () => auth.signInWithPopup(provider);

export default firebase;